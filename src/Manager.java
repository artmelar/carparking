import java.util.Scanner;
import java.util.Random;

public class Manager {

    static Parking parking = new Parking();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Введите количество парковочных мест для легковых машин");
        int places = scanner.nextInt();
        System.out.println("Введите количество парковочных мест ждя грузовых машин");
        int truckPlaces = scanner.nextInt();
        parking.setCapacity(places);
        parking.setFreePlaces(places);
        parking.cars = new Car[parking.getCapacity()];
        parking.setTruckCapacity(truckPlaces);
        parking.setFreeTruckPlaces(truckPlaces);
        parking.trucks = new Truck[parking.getTruckCapacity()];
        int allParkingPlaces = places + truckPlaces;
        boolean checkUserChoice1 = false;
        while (checkUserChoice1 == false) {
            System.out.println();
            System.out.println("Что вы хотите сделать?");
            System.out.println("0 - перейти к следующему дню");
            System.out.println("1 - узнать количество свободных мест, занятых мест");
            System.out.println("2 - показать парковку");
            System.out.println("3 - закрыть парковку");
            int command = scanner.nextInt();
            if ((command == 0) || (command == 1) || (command == 2) || (command == 3)) {
                switch (command) {
                    case 0:
                        nextDay(1);
                        int count1 = random.nextInt(allParkingPlaces / 3);
                        while (count1 > 0) {
                            if (parking.getFreePlaces() > 0) {
                                parking.parkCar();
                                count1--;
                            } else {
                                System.out.println("Мест на парковке легковых машин больше нет");
                                break;
                            }
                        }
                        int count2 = random.nextInt(allParkingPlaces / 3);
                        while (count2 > 0) {
                            if (parking.getFreePlaces() > 0) {
                                parking.parkTruck();
                                count2--;
                            } else {
                                break;
                            }
                        }
                        break;
                    case 1:
                        System.out.println(parking.info());
                        break;
                    case 2:
                        parking.showCars();
                        parking.showTrucks();
                        break;
                    case 3:
                        System.out.println("Парковка закрыта!");
                        checkUserChoice1 = true;
                        break;

                }
            }
        }
    }

    private static void nextDay(int days) {
        for (int i = 0; i < days; i++) {
            parking.setDay(parking.getDay() + 1);
            parking.checkParkingTimeExpired();
        }
    }
}