import java.util.Random;

public class Truck extends Car {

    public Truck(int daySinceStay,int dayToStay) {
        this.daysToStay = dayToStay;
        this.daySinceStay = daySinceStay;
        this.daysLeft = this.daysToStay;
        this.parkingTimeExpired = false;
        this.number = -1;
    }

    public Truck(int daySinceStay) {
        Random random = new Random();
        this.daysToStay = 3 + random.nextInt(15);
        this.daySinceStay = daySinceStay;
        this.daysLeft = this.daysToStay;
        this.parkingTimeExpired = false;
        this.number = 1000 + random.nextInt(8999);
    }

    public String truckInfo() {

        return  getNumber() + " припаркован еще на " + daysLeft + " дней ";
    }
}
