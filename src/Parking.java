public class Parking {

    int capacity;
    int busyPlaces;
    int freePlaces;
    int truckCapacity;
    int busyTruckPlaces;
    int freeTruckPlaces;
    int day;
    Car[] cars;
    Car[] places;
    Truck[] trucks;
    Truck[] spaces;

    public Parking() {
        this.capacity = capacity;
        this.freePlaces = capacity;
        this.busyPlaces = 0;
        this.busyTruckPlaces = 0;
        this.truckCapacity = truckCapacity;
        this.freeTruckPlaces = truckCapacity;
    }

    public int getTruckCapacity() {
        return truckCapacity;
    }

    public void setTruckCapacity(int truckCapacity) {
        this.truckCapacity = truckCapacity;
    }

    public int getBusyTruckPlaces() {
        return busyTruckPlaces;
    }

    public void setBusyTruckPlaces(int busyTruckPlaces) {
        this.busyTruckPlaces = busyTruckPlaces;
    }

    public int getFreeTruckPlaces() {
        return freeTruckPlaces;
    }

    public void setFreeTruckPlaces(int freeTruckPlaces) {
        this.freeTruckPlaces = freeTruckPlaces;
    }

    public int getBusyPlaces() {
        return busyPlaces;
    }

    public void setBusyPlaces(int busyPlaces) {
        this.busyPlaces = busyPlaces;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

    public void parkCar() {
        if (freePlaces > 0) {
            cars[busyPlaces] = new Car(day);
            System.out.println("На парковку приехала легковая машина: " + cars[busyPlaces].getNumber());
            busyPlaces++;
            freePlaces--;
        } else {
            System.out.println("Свободных мест на парковке для легковых машин нет");
            System.out.println("Место освободится через " + findFreeCarPlace() + " дней ");
        }
    }

    public void parkTruck() {
        if (freeTruckPlaces > 0) {
            trucks[busyTruckPlaces] = new Truck(day);
            System.out.println("На парковку приехала грузовая машина: " + trucks[busyTruckPlaces].getNumber());
            busyTruckPlaces++;
            freeTruckPlaces--;
        } else {
            System.out.println("Свободных мест на парковке для грузовых машин нет ");
            System.out.println("Место освободится через " + findFreeTruckPlace() + " дней ");
            parkTruckOnCarPlaces(day);
        }
    }

    public void parkTruckOnCarPlaces(int day) {
        if (freePlaces > 1) {
            Truck truck = new Truck(day);
            cars[busyPlaces] = truck;
            busyPlaces++;
            freePlaces--;
            cars[busyPlaces] = new Truck(day, truck.getDaysToStay());
            busyPlaces++;
            freePlaces--;
            System.out.println("На парковку легковых машин приехал грузовик: " + truck.getNumber() );

        } else {
            System.out.println("Свободных мест на парковке для легковых машин нет");
            System.out.println("Место освободится через " + findFreeTruckPlace() + " дней)");
        }
    }

    private int findFreeCarPlace() {
        int days = cars[0].getDaysLeft();
        for (int i = 1; i < busyPlaces - 1; i++) {
            if (days > cars[i].getDaysLeft()) {
                days = cars[i].getDaysLeft();
            }
        }
        return days;
    }

    private int findFreeTruckPlace() {
        int days = trucks[0].getDaysLeft();
        for (int i = 1; i < busyTruckPlaces - 1; i++) {
            if (days > trucks[i].getDaysLeft()) {
                days = trucks[i].getDaysLeft();
            }
        }
        return days;
    }

    public void removeCar(int id) {
        cars[id].setParkingTimeExpired(true);
        if (cars[id] instanceof Truck) {
            if (cars[id].getNumber() == -1) {
                cars[id - 1].setParkingTimeExpired(true);
            } else cars[id + 1].setParkingTimeExpired(true);
        }
        sortCars();
    }

    public void removeTruck(int id) {
        trucks[id].setParkingTimeExpired(true);
        sortTrucks();
    }

    public void sortCars() {
        for (int i = 0; i < busyPlaces - 1; i++) {
            if (cars[i].getParkingTimeExpired()) {

                for (int j=i; j < busyPlaces - 1; j++) {
                    places[j] = cars[j];
                    cars[j] = cars[j + 1];
                    cars[j + 1] = places[j];
                }
                if(cars[busyPlaces-2].getParkingTimeExpired()){break;}
                i--;
            }
        }
        for (int i = busyPlaces - 1; i > 0; i--) {
            if (cars[i].getParkingTimeExpired()) {
                if (cars[i].getNumber() != -1) {
                    System.out.println("С парковки уехал: " + cars[i].getNumber());
                }
                cars[i] = null;
                busyPlaces--;
                freePlaces++;
            } else return;
        }
    }

    public void sortTrucks() {
        for (int i = 0; i < busyTruckPlaces - 1; i++) {
            if (trucks[i].getParkingTimeExpired()) {
                for (int j = i; j < busyTruckPlaces - 1; j++) {
                    spaces[j] = trucks[j];
                    trucks[j] = trucks[j + 1];
                    trucks[j + 1] = spaces[j];
                }
            }
        }
        for (int i = busyTruckPlaces - 1; i > 0; i--) {
            if (trucks[i].getParkingTimeExpired()) {
                System.out.println("С парковки уехал: " + trucks[i].getNumber());
                trucks[i] = null;
                busyTruckPlaces--;
                freeTruckPlaces++;
            } else return;
        }
    }

    public void checkParkingTimeExpired() {
        for (int i = 0; i < busyPlaces; i++) {
            cars[i].setDaysLeft(cars[i].getDaysLeft() - 1);
            if (cars[i].getDaysLeft() <= 0) {
                removeCar(i);
            }
        }
        for (int i = 0; i < busyTruckPlaces; i++) {
            trucks[i].setDaysLeft(trucks[i].getDaysLeft() - 1);
            if (trucks[i].getDaysLeft() <= 0) {
                removeTruck(i);
            }
        }
    }

    public void showTrucks() {
        if (getBusyTruckPlaces() > 0) {
            for (int i = 0; i < getBusyTruckPlaces(); i++) {
                System.out.println(trucks[i].truckInfo() + " на " + (i+1) + " месте");
            }
        } else System.out.println("На парковке нет машин");
    }

    public void showCars() {
        if (getBusyPlaces() > 0) {
            for (int i = 0; i < getBusyPlaces(); i++) {
                if (cars[i] instanceof Truck) {
                    System.out.print("на " + i + " и ");
                    i++;
                    System.out.println((i) + " местах " + cars[i-1].carInfo());
                } else {
                    System.out.println(cars[i].carInfo() + " на " + i + " месте");
                }
            }
        } else System.out.println("На парковке нет машин");
    }

    public String info() {
        return "Парковка" +
                " имеет: " + capacity +
                " мест для легковых и " + truckCapacity + " мест для грузовых, текущее состояние парковки: "
                + freePlaces + " свободных мест для легковых и " + freeTruckPlaces +
                " свободных мест для грузовых, " + busyPlaces + " занятых мест для легковых, " + busyTruckPlaces +
                " занятых мест для грузовых";

    }
}