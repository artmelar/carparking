import java.util.Random;

public class Car {

    int number;
    int daysToStay;
    int daySinceStay;
    int daysLeft;
    boolean parkingTimeExpired;

    public Car() {
    }

    public Car(int daySinceStay) {
        Random random = new Random();
        this.daysToStay = 1 + random.nextInt(12);
        this.daySinceStay = daySinceStay;
        this.daysLeft = this.daysToStay;
        this.parkingTimeExpired = false;
        this.number = 1000 + random.nextInt(8999);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDaysToStay() {
        return daysToStay;
    }

    public void setDaysToStay(int daysToStay) {
        this.daysToStay = daysToStay;
    }

    public int getDaySinceStay() {
        return daySinceStay;
    }

    public void setDaySinceStay(int daySinceStay) {
        this.daySinceStay = daySinceStay;
    }

    public int getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft;
    }

    public boolean getParkingTimeExpired() {
        return parkingTimeExpired;
    }

    public void setParkingTimeExpired(boolean toBeRemoved) {
        parkingTimeExpired = toBeRemoved;
    }

    public String carInfo() {

        return  getNumber() + " припаркован еще на " + daysLeft + " дней";
    }
}

